﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk02_ex05
{
    class Program
    {
        static void Main(string[] args)
        {
            var intro = "See how many questions you can get right by typing true or false";
            var question1 = $"5 + 5 = 10";
            var question2 = $"3 x 2 = 8";
            var question3 = $"6 + 1 = 7";
            var question4 = $"8 - 3 = 4";
            var question5 = $"4 + 2 = 7";
            var score = 0;
            var correct = true;

            Console.WriteLine($"{intro}");

            if (correct)
            {
                Console.WriteLine($"{question1}");
                if (Console.ReadLine() == "true")
                {
                    score += 1;
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"Bad luck, you suck. Your score is {score}");
                    correct = false;
                }
            }
            if (correct)
            {
                Console.WriteLine($"{question2}");
                if (Console.ReadLine() == "false") 
                {
                    score += 1;
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"Bad luck, you suck. Your score is {score}");
                    correct = false;
                }
            }
            if (correct)
            {
                Console.WriteLine($"{question3}");
                if (Console.ReadLine() == "true")
                {
                    score += 1;
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"Bad luck, you suck. Your score is {score}");
                    correct = false;
                }
            }
            if (correct)
            {
                Console.WriteLine($"{question4}");
                if (Console.ReadLine() == "false")
                {
                    score += 1;
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"Bad luck, you suck. Your score is {score}");
                    correct = false;
                }
            }
            if (correct)
            {
                Console.WriteLine($"{question5}");
                if (Console.ReadLine() == "false")
                {
                    score += 1;
                    correct = true;
                }
                else
                {
                    Console.WriteLine($"Bad luck, you suck. Your score is {score}");
                    correct = false;
                }

            }
            if (correct)
            {
                Console.WriteLine($"Well done, you're not dumb. You got all {score} questions correct.");
            }

            }
        
        }
    }

